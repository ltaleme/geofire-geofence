package asd.geo.geofire_geofence.other

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil

/**
 * Created by leult_000 on 2017-12-17.
 */

object Utils {

    //    val usersDBTag = "users"
    val locationsDBTag = "locations"
//    val userStatusOnline = "online"

    val permissionRequestCode = 7171;
    val playServicesResolutionRequest = 7172;


    val updateInterval = 5000;
    val fastestInterval = 3000;
    val distance = 1000;


    fun isLocationPermissionAccepted(context: Context): Boolean
            = (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            && (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)


    fun checkPlayServices(activity: Activity): Boolean {
        val resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity, Utils.playServicesResolutionRequest).show()
            } else {
                Toast.makeText(activity, "This device is not supported :(", Toast.LENGTH_SHORT).show()
                activity.finish()
            }
            return false
        }
        return true
    }

    private fun deg2rad(deg: Double): Double = deg * Math.PI / 180.0


    fun calculateDistance(myLocation: android.location.Location, friendLocation: android.location.Location): Double {
        val delta = myLocation.longitude - friendLocation.longitude

        var dist = Math.sin(deg2rad(myLocation.latitude)) * Math.sin(deg2rad(friendLocation.latitude)) * Math.cos(deg2rad(myLocation.latitude)) * Math.cos(deg2rad(friendLocation.latitude)) * delta

        return rad2deg(Math.acos(dist)) * 60 * 1.1515
    }

    private fun rad2deg(num: Double): Double = num * 180 / Math.PI

}
